#!/usr/bin/env python

#   Copyright (c) Joerg Wunsch
#   All rights reserved.
#
#   Redistribution and use in source and binary forms, with or without
#   modification, are permitted provided that the following conditions
#   are met:
#
#   * Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright
#     notice, this list of conditions and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the authors nor the names of its contributors
#     may be used to endorse or promote products derived from this software
#     without specific prior written permission.
#
#   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
#   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
#   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
#   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
#   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
#   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
#   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
#   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
#   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#   POSSIBILITY OF SUCH DAMAGE.

# Python equivalent of the 8-bit hash algorithm from sniffer_ctrl.c

import sys

def usage():
    sys.stderr.write("usage: genhash.py <word ...>\n")
    sys.exit(1)

def hash(cmd):
    h = 0
    accu = 0
    for c in cmd:
        c = ord(c)    # make integer
        accu = h & 0xE0
        h = h << 5
        h = h ^ (accu >> 5)
        h = h ^ c
        h = h & 0xFF
    return h

# main goes here
if len(sys.argv) <= 1:
    usage()

hashes = []
values = {}
for cmd in sys.argv[1:]:
    h = hash(cmd)
    hashes.append(h)
    values[h] = cmd

hashes.sort()

for h in hashes:
    cmd = values[h]
    print(f"    /** Hashvalue for command '{cmd}' */")
    cmd = cmd.upper()
    print(f"    CMD_{cmd:s} = 0x{h:02x},")
