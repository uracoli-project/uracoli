"""
    Test module HBM251

    The system calls are mocked using unittest.mock module

    unittest.mock is part of standard library for Python > 3.3, all others
    install with > pip install mock
"""

import os
import sys
import unittest

try:
    # Python 3 standard library
    import unittest.mock as mock
# except ModuleNotFoundError:
except:
    # Python 2, if mock installed via > pip install mock
    import mock

import tempfile

sys.path.append('.\Tools')
import hbm251

class Test_HBM251_Board(unittest.TestCase):

    # Fake setup config to avoid generation of config file
    setupconfig = {
        'port':'COM15', 
        'board':'radiofaro',
        'reset' : 'atprogram -t jtagice3 -i jtag -d atmega1284rfr2 -dc 3,0,12,0 -xr reset',
        'flvfy' : '',
        'flwrt' : 'atprogram -t jtagice3 -i jtag -d atmega1284rfr2 -dc 3,0,12,0 -xr chiperase program -f %(fw)s',
        'fltmo' : 0.1
        }

    # board config is optional, test both variants!
    # this is a copy of the config section of board radiofaro

    boardconfig = {
            'comment' : 'RadioFaro, Arduino like board with deRFmega128-22A001',
            'image' : 'radiofaro.jpg',
            'url' : 'http://www.radiofaro.de',
            'include' : 'boards/board_radiofaro.h',
            'cpu' : 'atmega128rfa1',
            'bootoffset' : '0x1e000',
            'f_cpu' : '16000000UL',
            'baudrate' : '57600',
            'lfuse' : '0xf7',
            'hfuse' : '0x9a',
            'efuse' : '0xfe',
            'sensors' : 'mcu_vtg mcu_t trxvtg',
            'provides' : 'trx led hif tmr rtc'
            }

    def setUp(self):
        # imitate the creation in hbm251 class constructor
        d=self.boardconfig
        d.update(self.setupconfig)
        
        self.tmpfw = tempfile.NamedTemporaryFile()
        self.tmpdir = tempfile.mkdtemp()

        d['device'] = 'atmega128' # TODO: where does HBM251 get it from?
        self.brd = hbm251.Board(d)
        self.exmock = mock.Mock(return_value=(0,'foo','bar'))
        hbm251.execute = self.exmock

    def tearDown(self):
        self.tmpfw.close()

    def test_reset(self):
        """Test the reset command"""
        self.brd.reset()

        # 0, 1 are default arguments of hbm251.execute()
        self.exmock.assert_called_with(self.brd['reset'], 0)

    def test_flash(self):
        """Test the flash command"""
        self.brd.flash(self.tmpfw.name)
        d={'fw':self.tmpfw.name}

        # 0, 1 are default arguments of hbm251.execute()
        self.exmock.assert_called_with(self.brd['flwrt'] % d, 0, 1)

    def test_find_app(self):
        """Test function for finding app"""
        fw = self.tmpfw.name

        # test giving any absolute path file, must return the filename itself
        self.assertEqual(self.brd.find_app(fw), fw)

        ## create file and test finding app of style {APP}_{BOARD}.hex
        tmpdir = tempfile.mkdtemp()
        self.brd['bindir'] = tmpdir
        resname = os.path.join(tmpdir, 'test_radiofaro.hex')

        # create a dummy file
        fhnd = open(resname, 'wb')
        fhnd.write(bytes([1,2,3]))
        fhnd.close()

        self.assertEqual(self.brd.find_app('test'), os.path.join(tmpdir, resname))

        # cleanup
        os.remove(resname) # remove the file first
        os.rmdir(tmpdir) # then the directory

if __name__ == '__main__':
    unittest.main(verbosity=2)
# EOF
